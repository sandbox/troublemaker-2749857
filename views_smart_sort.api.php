<?php

/**
 * Allows module to alter the sort value for a given node.
 *
 * @param $sort_value
 *    The sort value to alter.
 * @param $node
 *    The node which we're providing the sort value for.
 */
function hook_vss_alter(&$sort_value, $node) {
}

/**
 * Allows modules to alter the display of the sort value.
 *
 * @param mixed $view_value
 *    The view value to alter. Should be in a format accepted by drupal_render
 *    or FALSE if the field should not be displayed..
 * @param int $vss
 *    The raw VSS value.
 * @param $node
 *    The node that we're dispalying the VSS value for.
 */
function hook_vss_view_alter(&$view_value, $vss, $node) {

}
