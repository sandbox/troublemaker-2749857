<?php

/**
 * Form callback for Views smart sort configuration form.
 */
function views_smart_sort_config($form, &$form_state) {

  $form = array();
  $form['label_config'] = array();
  $form['#tree'] = TRUE;

  $types = node_type_get_types();
  $settings = variable_get('views_smart_sort', array());

  foreach ($types as $type) {
    $type_type = $type->type;
    $type_label = $type->name;
    $form['smart_sort'][$type_type] = array();
    $form['smart_sort'][$type_type]['type'] = array(
      '#markup' => $type_label
    );
    $form['smart_sort'][$type_type]['sort'] = array(
      '#type' => 'select',
      '#options' => _views_smart_sort_get_bundle_date_fields($type_type),
      '#default_value' => isset($settings[$type_type]) ? $settings[$type_type]['field'] . '.' . $settings[$type_type]['column'] : 'changed',
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save configuration',
  );
  return $form;

}

/**
 * Views smart sort configuration form submit callback.
 * @see views_smart_sort_config
 */
function views_smart_sort_config_submit($form, &$form_state) {
  $smart_sort_settings = array();
  foreach ($form_state['values']['smart_sort'] as $type => $config) {
    $config_items = explode('.', $config['sort']);
    $smart_sort_settings[$type] = array(
      'field' => $config_items[0],
      'column' => $config_items[1],
    );
  }
  variable_set('views_smart_sort', $smart_sort_settings);
  drupal_set_message('Configuration saved');
}

/**
 * Views smart sort config form theme function.
 * @see views_smart_sort_config
 */
function theme_views_smart_sort_config($variables) {
  $rows = array();
  $form = $variables['form'];
  foreach (element_children($form['smart_sort']) as $item_key) {
    $config_element = &$form['smart_sort'][$item_key];
    $rows[] = array(
      'data' => array(
        drupal_render($config_element['type']),
        drupal_render($config_element['sort']),
      ),
    );
  }
  return theme_table(array(
      'header' => array('Content type', 'Sort by'),
      'rows' => $rows,
      'attributes' => array(),
      'caption' => t(''),
      'empty' => t('Please create a content type to use this feature.'),
      'colgroups' => array(),
      'sticky' => TRUE,
    )
  ) . drupal_render_children($form);
}

/**
 * Get the sort options for a given node bundle.
 */
function _views_smart_sort_get_bundle_date_fields($bundle_type) {
  $sort_options = array('changed.value' => 'Last updated','created.value' => 'Created',);
  foreach (field_info_instances('node', $bundle_type) as $field_info_instance) {
    $field_info = field_info_field($field_info_instance['field_name']);
    if ($field_info['module'] == 'date') {
      if (in_array($field_info['settings']['todate'], array('optional','required'))) {
        $sort_options[$field_info_instance['field_name'] . '.value'] = $field_info_instance['label'] . ' (start)';
        $sort_options[$field_info_instance['field_name'] . '.value2'] = $field_info_instance['label'] . ' (end)';
      } else {
        $sort_options[$field_info_instance['field_name'] . '.value'] = $field_info_instance['label'];
      }
    }
  }
  return $sort_options;
}

