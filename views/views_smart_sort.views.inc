<?php

function views_smart_sort_views_data_alter(&$data) {
    $data ['node']['vss'] = array(
        'title' => t('Smart sort'),
        'help' => t('A field to sort across content types.'),
        'field' => array('handler' => 'views_handler_field'),
        'sort' => array('handler' => 'views_handler_sort_date'),
    );
}
